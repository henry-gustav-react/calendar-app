import React, { useEffect, useState } from 'react'
import Modal from 'react-modal'
import DateTimePicker from 'react-datetime-picker'
import moment from 'moment'
import Swal from 'sweetalert2'
import { useDispatch, useSelector } from 'react-redux'
import { uiCloseModal } from '../../actions/ui'
import {
  eventAddNew,
  eventClearActiveEvent,
  eventUpdated
} from '../../actions/events'

const now = moment()
  .minutes(0)
  .seconds(0)
  .add(1, 'hours') //3:45:50

const nowPlus1 = now.clone().add(1, 'hours')
const initEvent = {
  title: '',
  notes: '',
  start: now.toDate(),
  end: nowPlus1.toDate()
}
Modal.setAppElement('#root')

const CalendarModal = () => {
  const { modalOpen } = useSelector(state => state.ui)
  const { activeEvent } = useSelector(state => state.calendar)
  const dispatch = useDispatch()

  const [dateStart, setdateStart] = useState(now.toDate())
  const [dateEnd, setdateEnd] = useState(nowPlus1.toDate())
  const [titleValid, settitleValid] = useState(true)
  const [formValues, setformValues] = useState(initEvent)

  const { notes, title, start, end } = formValues

  useEffect(() => {
    if (activeEvent) {
      setformValues(activeEvent)
    } else {
      setformValues(initEvent)
    }
  }, [activeEvent, setformValues])

  const handleInputChange = ({ target }) => {
    setformValues({
      ...formValues,
      [target.name]: target.value
    })
  }
  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)'
    }
  }

  const handleChangeStartDate = e => {
    setdateStart(e)
    setformValues({
      ...formValues,
      start: e
    })
  }

  const handleChangeEndDate = e => {
    setdateEnd(e)
    setformValues({
      ...formValues,
      end: e
    })
  }
  const handleSubmitForm = e => {
    e.preventDefault()
    const momentStart = moment(start)
    const momentEnd = moment(end)
    if (momentStart.isSameOrAfter(momentEnd)) {
      console.log('Fecha dos debe se mayor')
      Swal.fire(
        'Error',
        'La fecha de fin debe ser mayor a la fecha de inicio',
        'error'
      )
      return
    }
    if (title.trim().length < 2) {
      return settitleValid(false)
    }
    //TODO : guardar en BD
    // console.log(formValues)
    if (activeEvent) {
      dispatch(eventUpdated(formValues))
    } else {
      dispatch(
        eventAddNew({
          ...formValues,
          id: new Date().getTime(),
          user: {
            _id: '123',
            name: 'Fernando'
          }
        })
      )
    }
    settitleValid(true)
    closeModal()
  }

  const closeModal = () => {
    dispatch(uiCloseModal())
    dispatch(eventClearActiveEvent())
    setformValues(initEvent)
  }
  return (
    <Modal
      isOpen={modalOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={closeModal}
      style={customStyles}
      closeTimeoutMS={500}
      className='modal'
      overlayClassName='modal-fondo'
    >
      {activeEvent ? <h1> Editar Evento</h1> : <h1> Nuevo evento </h1>}
      <hr />
      <form className='container' onSubmit={handleSubmitForm}>
        <div className='form-group'>
          <label>Fecha y hora inicio</label>
          <DateTimePicker
            onChange={handleChangeStartDate}
            value={dateStart}
            className='form-control'
          />
        </div>

        <div className='form-group'>
          <label>Fecha y hora fin</label>
          <DateTimePicker
            onChange={handleChangeEndDate}
            value={dateEnd}
            minDate={dateStart}
            className='form-control'
          />
        </div>

        <hr />
        <div className='form-group'>
          <label>Titulo y notas</label>
          <input
            type='text'
            className={`form-control ${!titleValid && 'is-invalid'}`}
            placeholder='Título del evento'
            name='title'
            autoComplete='off'
            value={title}
            onChange={handleInputChange}
          />
          <small id='emailHelp' className='form-text text-muted'>
            Una descripción corta
          </small>
        </div>

        <div className='form-group'>
          <textarea
            type='text'
            className='form-control'
            placeholder='Notas'
            rows='5'
            name='notes'
            value={notes}
            onChange={handleInputChange}
          ></textarea>
          <small id='emailHelp' className='form-text text-muted'>
            Información adicional
          </small>
        </div>

        <button type='submit' className='btn btn-outline-primary btn-block'>
          <i className='far fa-save'></i>
          <span> Guardar</span>
        </button>
      </form>
    </Modal>
  )
}

export default CalendarModal
