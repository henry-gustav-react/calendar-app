import React, { useState } from 'react'
import Navbar from '../ui/Navbar'
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import 'moment/locale/es'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import { messages } from '../../helpers/calendar-messages-es'
import CalendarEvent from './CalendarEvent'
import CalendarModal from './CalendarModal'
import { useDispatch, useSelector } from 'react-redux'
import { uiopenModal } from '../../actions/ui'
import { eventClearActiveEvent, eventSetActive } from '../../actions/events'
import AddNewFab from '../ui/AddNewFab'
import DeleteEventFab from '../ui/DeleteEventFab'
const localizer = momentLocalizer(moment) // or globalizeLocalizer
moment.locale('es')
// const events = [
//   {
//     title: 'Cumpleanios',
//     start: moment().toDate(),
//     end: moment()
//       .add(2, 'hours')
//       .toDate(),
//     bgcolor: '#fafafa',
//     notes: 'comprar pastel',
//     user: {
//       _id: '123',
//       name: 'Henry'
//     }
//   }
// ]

const CalendarScreen = () => {
  const dispatch = useDispatch()
  const { events, activeEvent } = useSelector(state => state.calendar)
  const [lastView, setlastView] = useState(
    localStorage.getItem('lastView') || 'month'
  )
  const onDoubleClick = e => {
    console.log(e)
    dispatch(uiopenModal())
  }

  const onSelectEvent = e => {
    // console.log(e)
    dispatch(eventSetActive(e))
    // dispatch(uiopenModal())
  }

  const onSelectedSlot = e => {
    console.log(e)
    dispatch(eventClearActiveEvent(e))
  }

  const onViewChange = e => {
    // console.log(e)
    setlastView(e)
    localStorage.setItem('lastView', e)
  }
  const eventStyleGetter = (event, start, isSelected) => {
    // console.log(event, start, isSelected)

    const style = {
      backgroundColor: '#367CF7',
      borderRadius: 'Opx',
      opacity: 0.8,
      display: 'block'
    }
    return {
      style
    }
  }
  return (
    <div className='calendar-screen'>
      <Navbar />

      <Calendar
        localizer={localizer}
        events={events}
        startAccessor='start'
        endAccessor='end'
        messages={messages}
        eventPropGetter={eventStyleGetter}
        onDoubleClickEvent={onDoubleClick}
        onSelectEvent={onSelectEvent}
        onSelectSlot={onSelectedSlot}
        selectable={true}
        view={lastView}
        onView={onViewChange}
        components={{ event: CalendarEvent }}
      />

      <AddNewFab />
      {activeEvent && <DeleteEventFab />}
      <CalendarModal />
    </div>
  )
}

export default CalendarScreen
