import React from 'react'
import { useDispatch } from 'react-redux'
import { uiopenModal } from '../../actions/ui'

const AddNewFab = () => {
  const dispatch = useDispatch()

  const handleClickNew = () => {
    dispatch(uiopenModal())
  }
  return (
    <button className='btn btn-primary fab' onClick={handleClickNew}>
      <i className='fas fa-plus'></i>
    </button>
  )
}

export default AddNewFab
