import { types } from "../types/types"

export const uiopenModal = () => {
    return {
      type: types.uiOpenModal
    }
  }

  export const uiCloseModal = () => {
    return {
      type: types.uiCloseModal
    }
  }